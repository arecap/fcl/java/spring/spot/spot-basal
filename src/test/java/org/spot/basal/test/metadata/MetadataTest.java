/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.test.metadata;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.spot.basal.boot.BasalConfiguration;
import org.spot.basal.boot.BasalJpaConfiguration;
import org.spot.basal.metadata.Attribute;
import org.spot.basal.metadata.Element;
import org.spot.basal.metadata.MetaAttributeFactory;
import org.spot.basal.metadata.MetaElementFactory;
import org.spot.basal.repository.AttributeRepository;
import org.spot.basal.repository.ElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource("classpath:spot.basal.test.properties")
@ContextConfiguration(classes = { BasalJpaConfiguration.class, BasalConfiguration.class})
@Transactional
public class MetadataTest {


    @Autowired
    private MetaElementFactory metaObjectFactory;

    @Autowired
    private MetaAttributeFactory metaElementFactory;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private ElementRepository elementRepository;

    @Test
    public void testMetadataPersistence() {
        final Set<Element> persistedObjects = new LinkedHashSet<>();
        elementRepository.findAll().forEach(
                element -> {
                    persistedObjects.add(element);
                }
        );
        Assert.isTrue(metaElementFactory.getMetaAttributes().keySet().size() == persistedObjects.size());
        Assert.isTrue(metaObjectFactory.getMetaElements().size() == persistedObjects.size());
        for(Element persistedObject: persistedObjects) {
            Set<Attribute> objectStructure = metaElementFactory.getMetaElementAttributes(persistedObject.getHash());
            Assert.notNull(objectStructure);
            Assert.isTrue(objectStructure.size() == persistedObject.getAttributes().size());
        }
    }

}
