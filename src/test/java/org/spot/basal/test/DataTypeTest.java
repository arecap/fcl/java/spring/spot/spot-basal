/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.test;


import org.junit.Test;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class DataTypeTest {


    @Test
    public void dataTypeAssertTest() {

        Assert.isTrue(ClassUtils.isAssignable(Boolean.class, boolean.class) );
        Assert.isTrue(ClassUtils.isAssignable(Number.class, Long.class) );
        Assert.isAssignable(Collection.class, Set.class);
        Assert.isAssignable(Collection.class, List.class);
    }

}
