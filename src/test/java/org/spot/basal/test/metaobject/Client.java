/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.test.metaobject;


import org.spot.basal.annotation.ElementId;
import org.spot.basal.annotation.Metadata;
import org.spot.basal.datatype.Chronology;

import java.util.Date;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Metadata
public class Client {

    @ElementId
    private String hashId;

    @Metadata
    private String userAgent;

    @Metadata
    @ElementId
    private Iterable<User> users;

    @Metadata
    @Chronology
    private Date testDate;

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }
}
