/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.test.boot;


/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */

//@Configuration
//@TestPropertySource({"classpath:basal.test.datasource.properties"})
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "spotEntityManagerFactory",
//        transactionManagerRef = "spotTransactionManager")
public class SpotBasalJpaBootTest {

//    @Bean(name = "spotDataSource")
//    @ConfigurationProperties(prefix = "spot.datasource")
//    public DataSource spotDataSource() {
//        return DataSourceBuilder.create().build();
//    }
//
//    @Bean(name = "spotEntityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean spotEntityManagerFactory(EntityManagerFactoryBuilder builder,
//                                                               @Qualifier("spotDataSource") DataSource dataSource) {
//        return
//                builder
//                        .dataSource(dataSource)
//                        .packages("org.spot.basal")
//                        .persistenceUnit("spot")
//                        .build();
//    }
//
//    @Bean(name = "spotTransactionManager")
//    public PlatformTransactionManager spotTransactionManager(
//            @Qualifier("spotEntityManagerFactory") EntityManagerFactory spotEntityManagerFactory) {
//        return new JpaTransactionManager(spotEntityManagerFactory);
//    }

}
