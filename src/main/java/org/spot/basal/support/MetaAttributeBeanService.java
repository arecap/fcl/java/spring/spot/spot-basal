/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.spot.basal.metadata.*;
import org.spot.basal.repository.AttributeRepository;
import org.spot.basal.util.BasicDataTypeUtil;
import org.spot.basal.util.MetaElementUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldFilter;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class MetaAttributeBeanService implements MetaAttributeFactory, MetaAttributePostProcessor,
        InitializingBean, FieldFilter {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private final Map<String, Map<String, Attribute>> metaAttributes = new HashMap<>();

    private final MetaElementFactory metaElementFactory;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    public MetaAttributeBeanService(MetaElementFactory metaElementFactory) {
        this.metaElementFactory = metaElementFactory;
    }

    @Override
    public Map<String, Set<Attribute>> getMetaAttributes() {
        Map<String, Set<Attribute>> result = new LinkedHashMap<>();
        for(String elementHash: metaAttributes.keySet()) {
            Set<Attribute> elementAttributes = new LinkedHashSet<>();
            for(String attributeHash: metaAttributes.get(elementHash).keySet()) {
                elementAttributes.add(metaAttributes.get(elementHash).get(attributeHash));
            }
            result.put(elementHash, elementAttributes);
        }
        return result;
    }

    @Override
    public Set<Attribute> getMetaElementAttributes(String elementHash) {
        return getMetaAttributes().get(elementHash);
    }

    @Override
    public void postProcessMetaElements(Set<Element> elements) throws ClassNotFoundException {
        metaAttributes.clear();
        for(final Element element: elements) {
            ReflectionUtils
                    .doWithFields(Class.forName(element.getNamespace()+"."+ StringUtils.capitalize(element.getTitle())),
                    field -> {
                        queryMetaElementStructures(element);
                        postProcessMetaElementField(element, field);
                        },this);
        }
    }

    @Override
    public void postProcessMetaElementField(Element element, Field field) {
        Attribute attribute = getElementAttribute(element, field);
        if(metaAttributes.get(element.getHash()).get(attribute.getHash()) == null) {
            metaAttributes.get(element.getHash()).put(attribute.getHash(), attribute);
        }
    }

    @Override
    public boolean matches(Field field) {
        return MetaElementUtil.isMetaElement(field);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        postProcessMetaElements(metaElementFactory.getMetaElements());
    }

    protected void queryMetaElementStructures(Element element) {
        if(metaAttributes.get(element.getHash()) != null) {
            metaAttributes.get(element.getHash()).clear();
        }
        Set<Attribute> elementAttributes = attributeRepository.findAllByElementId(element.getId());
        Map<String, Attribute> metaElementAttributes = new HashMap<>();
        if(elementAttributes != null) {
            for(Attribute metaAttribute: elementAttributes) {
                metaElementAttributes.put(metaAttribute.getHash(), metaAttribute);
            }
        }
        metaAttributes.put(element.getHash(), metaElementAttributes);
    }

    protected Attribute getElementAttribute(Element element, Field field) {
        Attribute attribute = attributeRepository.findOneByElementIdAndName(element.getId(), field.getName());
        if(attribute == null) {
            attribute = new Attribute();
            attribute.setElement(element);
            attribute.setNamespace(element.getAttributeNamespace());
            attribute.setName(field.getName());
            attribute.setHash(MetaElementUtil.getHash(attribute.getNamespace(), attribute.getName()));
            attribute.setIdentifier(MetaElementUtil.isMetaIdentifier(field));
            attribute.setType(BasicDataTypeUtil.getFieldBasicDataType(field.getType()));
            return attributeRepository.save(attribute);
        }
        return attribute;
    }

//    protected Element getElement(Field metaElementField) {
//        Assert.isTrue(!BasicDataTypeUtil.isMap(metaElementField.getType()),
//                "Field ["+ metaElementField.getName() +"] definition not allowed!");
//        if(BasicDataTypeUtil.isCollection(metaElementField.getType())) {
//            return newCollectionElement(metaElementField);
//        }
//        if(BasicDataTypeUtil.isArray(metaElementField.getType())) {
//            return getTypeElement(metaElementField.getType().getComponentType(), metaElementField);
//        }
//        Element element = elementRepository.findOneByHash(MetaElementUtil.getFieldElementHash(metaElementField));
//        int basicDataType = BasicDataTypeUtil.getFieldBasicDataType(metaElementField);
//        Assert.isTrue(!(element == null && basicDataType == BasicDataType.OBJECT),
//                "Class ["+ metaElementField.getType() +" not @MetaElement annotated!");
//        if(element == null) {
//            return elementRepository.save(
//                    MetaElementUtil.newElement(metaElementField.getDeclaringClass().getCanonicalName(),
//                            metaElementField.getName(), basicDataType));
//        }
//        return element;
//    }
//
//    protected Element newCollectionElement(Field metaElementField) {
//        ParameterizedType genericType = (ParameterizedType) metaElementField.getGenericType();
//        Class<?> elementType = (Class<?>) genericType.getActualTypeArguments()[0];
//        return getTypeElement(elementType, metaElementField);
//    }
//
//    protected Element getTypeElement(Class<?> elementType, Field metaElementField) {
//        int basicDataType = BasicDataTypeUtil.getFieldBasicDataType(elementType);
//        if(basicDataType == BasicDataType.OBJECT) {
//            String elementHash = MetaElementUtil
//                    .getHash(ClassUtils.getPackageName(elementType),
//                            ClassUtils.getShortNameAsProperty(elementType));
//            Element element = elementRepository.findOneByHash(elementHash);
//            Assert.notNull(element, "Collection field ["+ metaElementField.getName()+"] not a Object Element!");
//            return element;
//        }
//        Assert.isTrue(BasicDataTypeUtil.isArray(elementType), "Field ["+ metaElementField.getName() +"] definition not allowed!");
//        return elementRepository.save(
//                MetaElementUtil.newElement(ClassUtils.getPackageName(metaElementField.getDeclaringClass()),
//                metaElementField.getName(), basicDataType));
//    }

}
