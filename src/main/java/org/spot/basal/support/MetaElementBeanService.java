/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.support;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.spot.basal.metadata.Element;
import org.spot.basal.metadata.MetaElementFactory;
import org.spot.basal.metadata.MetaElementPostProcessor;
import org.spot.basal.repository.ElementRepository;
import org.spot.basal.util.MetaElementUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.util.ClassUtils;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class MetaElementBeanService implements MetaElementFactory, MetaElementPostProcessor, InitializingBean {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    @Value("${spot.basal.basePackages}")
    private String[] basePackages;

    @Autowired
    private ElementRepository elementRepository;

    private final Map<String, Element> metaElements = new HashMap<>();

    @Override
    public Set<Element> getMetaElements() {
        Set<Element> objects = new LinkedHashSet<>();
        for(String hash: metaElements.keySet()) {
            objects.add(metaElements.get(hash));
        }
        return objects;
    }

    @Override
    public Element getMetaElement(String hash) {
        return metaElements.get(hash);
    }

    @Override
    public Element getMetaElement(String namespace, String title) {
        return getMetaElement(DigestUtils.md5DigestAsHex((namespace + "." + title).getBytes()));
    }

    @Override
    public void postProcessBasePackages(String... basePackages) throws ClassNotFoundException {
        queryObjectMap();
        for(String basePackage: basePackages) {
            postProcessCandidateComponents(
                    MetaElementUtil.getMetaElementComponentProvider().findCandidateComponents(basePackage));
        }
    }

    @Override
    public void postProcessCandidateComponents(Set<BeanDefinition> metaBeanDefinitions) throws ClassNotFoundException {
        for(BeanDefinition metaBeanDefinition: metaBeanDefinitions) {
            postProcessMetaElement(ClassUtils.getPackageName(metaBeanDefinition.getBeanClassName()),
                    ClassUtils.getShortNameAsProperty(Class.forName(metaBeanDefinition.getBeanClassName())));
        }
    }

    @Override
    public void postProcessMetaElement(String namespace, String title) {
        String elementHash = MetaElementUtil.getHash(namespace, title);
        if(metaElements.get(elementHash) == null) {
            Element object = elementRepository.findOneByHash(elementHash);
            if (object == null) {
                object = elementRepository.save(MetaElementUtil.newElement(namespace, title));
            }
            metaElements.put(object.getHash(), object);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        postProcessBasePackages(basePackages);
    }

    protected void queryObjectMap() {
        metaElements.clear();
        elementRepository.findAll().forEach(
                element -> {
                    metaElements.put(element.getHash(), element);
                });
    }

}
