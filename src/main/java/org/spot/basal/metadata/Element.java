/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.metadata;

import javax.persistence.*;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Entity
@Table(catalog = "spot", schema = "meta", name = "element")
public class Element {

    @Id
    @SequenceGenerator(catalog = "spot", schema = "meta", name = "element_element_id_seq", sequenceName = "element_element_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "element_element_id_seq")
    @Column(name = "element_id")
    private Long id;

    @Column(name = "hash", length = 32, unique = true)
    private String hash;

    @Column(name = "namespace")
    private String namespace;

    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "element", fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Attribute> attributes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    @Transient
    public String getAttributeNamespace() {
        return getNamespace() + "." + getTitle();
    }

}
