/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.metadata;

import javax.persistence.*;
import java.util.Set;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Entity
@Table(catalog = "spot", schema = "meta", name = "attribute")
public class Attribute {


    @Id
    @SequenceGenerator(catalog = "spot", schema = "meta", name = "attribute_attribute_id_seq", sequenceName = "attribute_attribute_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attribute_attribute_id_seq")
    @Column(name = "attribute_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "element_id", referencedColumnName = "element_id")
    private Element element;

    @Column(name = "hash", length = 32, unique = true)
    private String hash;

    @Column(name = "namespace")
    private String namespace;

    @Column(name = "name")
    private String name;

    @Column(name = "identifier")
    private boolean identifier;

    @Column(name = "type")
    private int type;

    @ManyToMany
    @JoinTable(catalog = "spot", schema = "meta", name = "attribute_link",
            joinColumns = {@JoinColumn(name = "a", referencedColumnName = "attribute_id")},
            inverseJoinColumns = {@JoinColumn(name = "b", referencedColumnName = "attribute_id")}
    )
    private Set<Attribute> links;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIdentifier() {
        return identifier;
    }

    public void setIdentifier(boolean identifier) {
        this.identifier = identifier;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Set<Attribute> getLinks() {
        return links;
    }

    public void setLinks(Set<Attribute> links) {
        this.links = links;
    }

}
