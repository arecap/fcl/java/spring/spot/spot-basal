/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.util;

import org.spot.basal.annotation.ElementId;
import org.spot.basal.annotation.Metadata;
import org.spot.basal.datatype.AttributeType;
import org.spot.basal.metadata.Element;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.DigestUtils;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public abstract class MetaElementUtil {

    public static boolean isMetaElement(AnnotatedElement annotatedElement) {
        return AnnotationUtils.findAnnotation(annotatedElement, Metadata.class) != null;
    }

    public static boolean isMetaIdentifier(AnnotatedElement annotatedElement) {
        return AnnotationUtils.findAnnotation(annotatedElement, ElementId.class) != null;
    }

    public static ClassPathScanningCandidateComponentProvider getMetaElementComponentProvider() {
        ClassPathScanningCandidateComponentProvider metaElementComponentProvider =
                new ClassPathScanningCandidateComponentProvider(false);
        metaElementComponentProvider.addIncludeFilter(new AnnotationTypeFilter(Metadata.class));
        return metaElementComponentProvider;
    }

    public static Element newElement(String namespace, String title) {
        Element structureObject = new Element();
        structureObject.setNamespace(namespace);
        structureObject.setTitle(title);
        structureObject.setHash(getHash(namespace, title));
        return structureObject;
    }

    public static String getHash(String namespace, String title) {
        return DigestUtils.md5DigestAsHex((namespace + "." + title).getBytes());
    }

    public static String getFieldElementNamespace(Field field) {
        if(BasicDataTypeUtil.getFieldBasicDataType(field) == AttributeType.OBJECT) {
            return ClassUtils.getPackageName(field.getType());
        }
        return ClassUtils.getPackageName(field.getDeclaringClass());
    }

    public static String getFieldElementTitle(Field field) {
        if(BasicDataTypeUtil.getFieldBasicDataType(field) == AttributeType.OBJECT) {
            return ClassUtils.getShortNameAsProperty(field.getType());
        }
        return field.getName();
    }

    public static String getFieldElementHash(Field field) {
        return getHash(getFieldElementNamespace(field), getFieldElementTitle(field));
    }


}
