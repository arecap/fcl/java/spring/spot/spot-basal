/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.util;

import org.spot.basal.datatype.AttributeType;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public abstract class BasicDataTypeUtil {


    public static int getFieldBasicDataType(Field field) {
        return getFieldBasicDataType(field.getType());
    }

    public static int getFieldBasicDataType(Class<?> type) {
        if(ClassUtils.isAssignable(Number.class, type)) {
            return AttributeType.NUMBER;
        }
        if(ClassUtils.isAssignable(String.class, type)) {
            return AttributeType.STRING;
        }
        if(ClassUtils.isAssignable(Boolean.class, type)) {
            return AttributeType.BOOLEAN;
        }
        if(isArray(type)) {
            return AttributeType.ARRAY;
        }
        return AttributeType.OBJECT;
    }

    public static boolean isCollection(Class<?> type) {
        return ClassUtils.isAssignable(Collection.class, type);
    }

    public static boolean isMap(Class<?> type) {
        return ClassUtils.isAssignable(Map.class, type);
    }

    public static boolean isArray(Class<?> type) {
        return ClassUtils.isAssignable(Collection.class, type) ||
                ClassUtils.isAssignable(Map.class, type) ||
                type.isArray();
    }

}
