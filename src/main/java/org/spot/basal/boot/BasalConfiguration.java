/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.boot;

import org.spot.basal.metadata.MetaAttributeFactory;
import org.spot.basal.metadata.MetaElementFactory;
import org.spot.basal.support.MetaAttributeBeanService;
import org.spot.basal.support.MetaElementBeanService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Configuration
@PropertySource("classpath:spot.basal.properties")
@ComponentScan("org.spot.basal")
public class BasalConfiguration implements InitializingBean {


    @Bean
    public static MetaElementFactory getMetaElementFactory() {
        return new MetaElementBeanService();
    }

    @Bean
    public static MetaAttributeFactory getMetaAttributeFactory(MetaElementFactory metaElementFactory) {
        MetaAttributeBeanService metaAttributeFactory = new MetaAttributeBeanService(metaElementFactory);
        return metaAttributeFactory;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
