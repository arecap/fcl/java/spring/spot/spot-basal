/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.datastore;

import org.spot.basal.metadata.Attribute;

import javax.persistence.*;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Entity
@Table(catalog = "spot", schema = "datastore", name = "segregate")
public class Segregate {


    @Id
    @SequenceGenerator(catalog = "spot", schema = "datastore", name = "segregate_segregate_id_seq", sequenceName = "segregate_segregate_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "segregate_segregate_id_seq")
    @Column(name = "segregate_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "attribute_id", referencedColumnName = "attribute_id")
    private Attribute attribute;

    @Column(name = "segregate_hash", length = 32)
    private String segregateHash;

    @ManyToOne
    @JoinColumn(name = "data_id", referencedColumnName = "data_id")
    private Data data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public String getSegregateHash() {
        return segregateHash;
    }

    public void setSegregateHash(String segregateHash) {
        this.segregateHash = segregateHash;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
