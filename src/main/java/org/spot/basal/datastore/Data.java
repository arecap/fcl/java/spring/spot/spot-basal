/*
 * Copyright 2017-2018 The original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.spot.basal.datastore;

import javax.persistence.*;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Entity
@Table(catalog = "spot", schema = "datastore", name = "datastore")
public class Data {


    @Id
    @SequenceGenerator(catalog = "spot", schema = "datastore", name = "data_data_id_seq", sequenceName = "data_data_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_data_id_seq")
    @Column(name = "data_id")
    private Long id;

    @Column(name = "storage_idx")
    private int storageIndex;

    @Column(name = "storage_pin")
    private int storagePin;

    @Column(name = "data_hash", length = 32)
    private String dataHash;

    @Column(name = "store", length = 10485760)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStorageIndex() {
        return storageIndex;
    }

    public void setStorageIndex(int storageIndex) {
        this.storageIndex = storageIndex;
    }

    public int getStoragePin() {
        return storagePin;
    }

    public void setStoragePin(int storagePin) {
        this.storagePin = storagePin;
    }

    public String getDataHash() {
        return dataHash;
    }

    public void setDataHash(String dataHash) {
        this.dataHash = dataHash;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
